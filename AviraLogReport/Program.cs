﻿using Hangfire;
using Hangfire.MemoryStorage;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace AviraLogReport
{
    public class Program
    {
        public static string path = @"C:\ProgramData\Avira\Antivirus\LOGFILES";

        public static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            GlobalConfiguration.Configuration.UseMemoryStorage();
            Console.WriteLine("Avira Free Antivirus Log Sent via Gmail");
            var host = Dns.GetHostEntry(Dns.GetHostName());
            Console.WriteLine("HostName: " + host.HostName);
            foreach (var address in host.AddressList)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                {
                    Console.WriteLine(address.ToString());
                }
            }
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("input ip hostname");
            string hostname = Console.ReadLine();

            Console.WriteLine("input ip address");
            string ipAddress = Console.ReadLine();

            Console.WriteLine("input log path");
            string logPath = Console.ReadLine();

            Console.WriteLine("input gmail smtp account");
            string username = Console.ReadLine();

            Console.WriteLine("input gmail smtp password");
            string password = Console.ReadLine();

            Console.WriteLine("input interval day");
            string intervalStr = Console.ReadLine();
            double interval = Convert.ToDouble(intervalStr);

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(Dns.GetHostEntry(Dns.GetHostName()).HostName + " BackgroundJob.Enqueue Start");

            BackgroundJob.Enqueue<Program>(x => x.MailNotify(hostname, logPath, ipAddress, username, password, interval));
            using (new BackgroundJobServer())
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Hangfire Server started. Press ENTER to exit...");
                Console.WriteLine(JobStorage.Current.GetMonitoringApi());
                Console.ForegroundColor = ConsoleColor.White;
                Console.ReadLine();
            }
        }

        public async Task MailNotify(string hostname, string path, string ipAddress, string username, string password, double interval)
        {
            Console.WriteLine("Add Schedule task");
            BackgroundJob.Schedule<Program>(x => x.MailNotify(hostname, path, ipAddress, username, password, interval), TimeSpan.FromDays(interval));
            try
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Start MailNotify");
                DirectoryInfo d = new DirectoryInfo(path);
                FileInfo[] files = d.GetFiles("AVSCAN-*");
                string logFile = path + "\\" + files.LastOrDefault();
                Console.WriteLine("file: " + logFile);

                using (var fs = new FileStream(logFile, FileMode.Open))
                {
                    TextWriter tw = new StreamWriter(fs);
                }
                Attachment attachment = new Attachment(logFile);

                string host = "smtp.gmail.com";
                string port = "25";
                bool enabledSsl = true;
                bool enabledCredential = true;

                SmtpClient smtp = new SmtpClient(host);
                smtp.UseDefaultCredentials = false;
                smtp.Port = Convert.ToInt32(port);
                smtp.EnableSsl = enabledSsl;
                if (enabledCredential)
                {
                    smtp.Credentials = new NetworkCredential(username, password);
                }
                smtp.Port = Convert.ToInt32(port);

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("grape778899@gmail.com", "Single Antivirus Log");
                //mail.To.Add("singleqiang@gmail.com");
                mail.To.Add("tp611239@gmail.com");
                mail.Subject = "av-" + hostname + "_" + ipAddress;
                mail.Body = "Avira Free Antivirus Log";
                mail.Attachments.Add(attachment);
                await smtp.SendMailAsync(mail);

                Console.WriteLine("sent success");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
